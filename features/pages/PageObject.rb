class PageObject < SitePrism::Page
  include Capybara::DSL
  include RSpec::Matchers

  def clicar(element)
    element.click
  end

  def btn_enter(element)
    element.native.send_keys(:enter)
  end

  def digitar(element, value)
    element.send_keys(value)
  end

  def selecionar_opcao(element, value)
    element.select(value)
  end

  # def click_botao(nome_botao)
  #   find("input[value='" + nome_botao + "'").click
  # end

  def clicar_botao(nome_botao)
    click_button (nome_botao)
  end

  def validar_mensagem(element, mensagem_feature)
    # wait_until_element_visible
    expect(element.text()).to include mensagem_feature
  end

  def validar_mensagem_sucesso(element, mensagem_feature)
    expect(element.text()).to include mensagem_feature
  end

  def validar_valor_elemento(element, value)
    expect(element.text()).to include value
  end

  def validar_contem_valor_elemento(element, value)
    expect(element).to have_content value
  end

  #valida a mensagem apresentada no elemento informado
  def texto_esperado(element, mensagem)
    expect(element.text()).to include mensagem
  end

  def clicar_combo(combo)
    combo.click
  end

  def clicar_opcao(opcao)
    opcao.click
  end

  def data_atual(element)
    data = Date.today
    data = data.strftime("%d%m%Y")
    element.send_keys(data)
  end

  #Método para ser utilizado nas páginas de avaliação completa e simplificada
  def inserir_anexo(arquivo)
    btn_selecionar = page.all("div table tbody td:nth-child(1) label input", visible: false).length

    i = 1
    while i <= btn_selecionar
      find("div table tbody tr:nth-child(#{i}) td:nth-child(1) label input", visible: false).set(arquivo)
      i += 1
    end
  end
end
