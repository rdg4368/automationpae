class Cadastro_Saida < SitePrism::Page
  set_url "/"

  #combo caixa
  element :combo_caixa, "#form-main > div:nth-child(2) > div > div > div > button > span.filter-option.pull-left"
  element :input_caixa, "#form-main > div:nth-child(2) > div > div > div > div > div > input"
  element :opcao_caixa, "#form-main > div:nth-child(2) > div > div > div > div > ul > li:nth-child(3) > a > span.text"

  #combo plano conta
  #clicar combo
  element :combo_plano_conta, "#form-main > div:nth-child(3) > div > div > div > button > span.filter-option.pull-left"
  #preencher o campo plano conta
  element :input_plano_conta, "#form-main > div:nth-child(3) > div > div > div > div > div > input"
  #seleciona a opcao
  element :opcao_plano_conta, "#form-main > div:nth-child(3) > div > div > div > div > ul > li.active > a > span.text"

  #input valor data pagante e descricao
  element :input_valor, "#id_valor"
  element :input_data_movimento, "#id_data_movimento"
  element :input_pagante_favorecido, "#id_pagante_favorecido"
  element :input_descricao, "#id_descricao"

  #btn_salvar
  element :btn_salvar_cadastro, "#form-main > div.form-group.no-print > div > button.btn.btn-success.btn-icon-text.waves-effect"

  #mensagem sucesso
  element :msg_sucesso, :xpath, "//span[3]"

  #não sei
  element :btn_saida, "#content > div > div.card > div.card-buttons > button.btn.btn-danger.waves-effect"
end
