class Login < SitePrism::Page
  set_url "/"

  #login e senha
  element :username, "#form-login > div:nth-child(2) > div > input"
  element :password, "#form-login > div:nth-child(3) > div > input"

  #botão submit

  element :btn_logar, "#sign-in"

  def openpage
    visit "/login/"
  end

  def login_senha
    username.send_keys("")
    password.send_keys("")
  end

  def btn_submeter
    btn_logar.click
  end
end
