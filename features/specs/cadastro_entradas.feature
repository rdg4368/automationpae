#language: pt

Funcionalidade: cadastro de dizimista com sucesso
    Eu como usuário do sistema quero acessar o sitema Pae para cadasto das entradas
    
Contexto: Login no Pae com sucesso
     Dado avaliador realizar login
#    Dado acessar sistema

    @cadastro_entrada
    Esquema do Cenário: cadastro de entradas

        Quando acessar o cadastro
        E clicar em entrada de lançamento
        E selecionar o nome do Caixa
        E informar o valor #"<valor>"
        E informar a data do movimento #"<datamovimento>"
        E informar o Pagante/Favorecido #"<pag/favorecido>"
        E informar a descrição #"<descricao>"
        E selecionar o Plano conta entrada #"<plano>"
        Quando submeter o cadastro
        Então o sistema deve apresentar a mensagem de sucesso

        Exemplos:
            | datamovimento | pag/favorecido | descricao                                | valor  | plano|
            | 01/03/2021    | NOTA           | COMPRA DO MATERIAL DO ALMOCO CONSAGRACAO | 262,00 | oferta culto    |

