#language: pt

Funcionalidade: cadastro de dizimista com sucesso
    Eu como usuário do sistema quero acessar o sitema Pae para cadasto das entradas
    
Contexto: Login no Pae com sucesso
     Dado avaliador realizar login no sistema

    @cadastro_saidas
    Esquema do Cenário: cadastro de saidas

        Quando acessar o cadastro de saidas
        E clicar em saida de lançamento
        E selecionar o nome do Caixa saida
        E informar o valor #"<valor>" saida
        E informar a data do movimento #"<datamovimento>" saida
        E informar o Pagante/Favorecido #"<pag/favorecido>" saida
        E informar a descrição #"<descricao>" saida
        E selecionar o Plano conta saida #"<plano>"
        Quando submeter o cadastro saida
        Então o sistema deve apresentar a mensagem de sucesso saida

        Exemplos:
            | valor   | datamovimento | pag/favorecido      | descricao                                         | plano |
            | 1500,00 | 01/03/2021    | Paulo Avelino  	    | PAGO AO PASTOR PAULO AVELINO DOS SANTOS PREBENDA	| DESPESAS PASTORAIS - Subsídios (prebendas)                                |
            | 1000,00 | 01/03/2021    | Paulo Avelino  	    | PAGO PARCELA DO CARRO PAULO AVELINO				| AQUISIÇÕES - Imóveis (predios, residencias, salas)                        |
            | 45,00   | 12/03/2021    | Maria Leidiane      | COMPRA DO MATERIAL ORNAMENTAÇÃO DA IGREJA			| 2.13.09 - EVENTOS - Comemorações e Festividades                           |
            | 300,00  | 12/03/2021    | Jorge Luiz  	    | PAGO AO DIACONO JORGE LIMPEZA IGREJA				| 2.01.10 - DESPESAS ADMINISTRATIVAS - Honorários e Prestação de Serviços   |
            | 70,00	  | 12/03/2021    | Sara Santana 	    | PAGO A EQUIPE DE ORNAMENTAÇÃO						| 2.13.09 - EVENTOS - Comemorações e Festividades                           |
            | 176,00  | 19/03/2021    |   				    | COMPRA DO MATERIAL MANUTENÇÃO DA IGREJA			| 2.05.04 - CONSERVAÇÃO - Manutenção de Instalações (pequenos reparos)      |
            | 134,00  | 20/03/2021    |  				    | PAGO NOTA FISCAL 218404 SERIE 06 					| 2.05.01 - CONSERVAÇÃO - Material de Limpeza e Higiene                     |
            | 75,00	  | 20/03/2021	  | Paulo Avelino	    | DEPOSITO 50% SGAS PAULO AVELINO					| 2.18.19 - SECRETARIAS - SGAS - 25% do FIW dos Clérigos                    |
            | 75,00	  | 20/03/2021	  | Paulo Avelino	    | DEPOSITO 50% FIW PAULO AVELINO 					| 2.16.11 - INSTITUIÇÕES - FIW - Fundo Imobiliário Wesleyano                |
            | 600,38  | 20/03/2021	  | Secretária finanças	|PAGO DE 15% SRF REF. AO MÊS DE JANEIRO COTA ORÇAMENTÁRIA | 2.18.01 - SECRETARIAS - SRF - Regional de Finanças (Cota Orçamentária)|
            | 40,00   | 21/03/2021	  | Gabriel Conz	    | OFERTA PREGADOR CULTO DE MISSÕES					| 2.09.03 - EVANGELISMO - Despesas com Obreiros e Preletores                |
            | 64,00   | 24/03/2021	  |                     |COMPRA DA ROUPA  EQUIPE DE DANÇA (PARCELA 2/3)     | 2.17.12 - MINISTÉRIOS - Dança                                             |
            | 60,00   | 25/03/2021	  | Paulo Avelino	    | COMBUSTÍVEL PASTOR PAULO                          | Combustível e Lubrificantes                                               |
            | 523,00  | 24/03/2021    | José Vidal		    | AÇÃO SOCIAL (AJUDA FINANCEIRA SEPUTAMENTO)        | 2.07.01 - AÇÃO SOCIAL - Ajuda Financeira                                  |
            | 30,00   | 29/03/2021	  | Josimar			    | AÇÃO SOCIAL (AJUDA FINANCEIRA COMPRA DE REMEDIOS) | 2.07.02 - AÇÃO SOCIAL - Medicamentos e Roupas                             |