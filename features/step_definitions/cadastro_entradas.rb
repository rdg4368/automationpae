Dado("avaliador realizar login") do
  @app.login.openpage
  @app.login.login_senha
  @app.login.btn_submeter
end

Quando("acessar o cadastro") do
  #@app.lancamento.menu
  visit "/financeiro/lancamento/incluir/entrada/"
end

Quando("clicar em entrada de lançamento") do
  #@app.lancamento.click_button "ENTRADA"
  #@app.lancamento.btn_entrada
end

Quando("selecionar o nome do Caixa") do
  @po = PageObject.new
  @cadastro_entrada = Cadastro_Entradas.new
  #--clica na combo-------
  @po.clicar_combo(@cadastro_entrada.combo_caixa)
  #--preencher o campo-------
  @po.digitar(@cadastro_entrada.input_caixa, "Buritis")
  #--seleciona a opcao-------
  @po.clicar_opcao(@cadastro_entrada.opcao_caixa)
end

Quando('informar o valor #{string}') do |valor|
  @po = PageObject.new
  @cadastro_entrada = Cadastro_Entradas.new
  @po.digitar(@cadastro_entrada.input_valor, valor)
end

Quando('informar a data do movimento #{string}') do |datamovimento|
  find("#id_data_movimento").set datamovimento
end

Quando('informar o Pagante\/Favorecido #{string}') do |pagante|
  @po = PageObject.new
  @cadastro_entrada = Cadastro_Entradas.new
  @po.digitar(@cadastro_entrada.input_pagante_favorecido, pagante)
end

Quando('informar a descrição #{string}') do |descricao|
  #@po = PageObject.new
  #@cadastro_entrada = Cadastro_Entradas.new
  @po.digitar(@cadastro_entrada.input_descricao, descricao)
end


Quando('selecionar o Plano conta entrada #{string}') do |plano|
  @po = PageObject.new
  @cadastro_entrada = Cadastro_Entradas.new
  @po.clicar_combo(@cadastro_entrada.combo_plano_conta)
  @po.digitar(@cadastro_entrada.input_plano_conta, plano)
  @po.clicar_opcao(@cadastro_entrada.opcao_plano_conta)

end

Quando("submeter o cadastro") do

  # botão submeter
  @po.clicar(@cadastro_entrada.btn_salvar_cadastro)
end

Então("o sistema deve apresentar a mensagem de sucesso") do
  @po = PageObject.new
  @cadastro_entrada = Cadastro_Entradas.new
  @po.validar_mensagem(@cadastro_entrada.msg_sucesso, "Lançamentos incluido com sucesso!")
end
