Dado("avaliador realizar login no sistema") do
  @app.login.openpage
  @app.login.login_senha
  @app.login.btn_submeter
end

Quando("acessar o cadastro de saidas") do
  #@app.lancamento.menu
  visit "/financeiro/lancamento/incluir/saida/"
end

Quando("clicar em saida de lançamento") do
  #@app.lancamento.click_button "ENTRADA"
  #@app.lancamento.btn_saida
end

Quando("selecionar o nome do Caixa saida") do
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  #--clica na combo-------
  @po.clicar_combo(@cadastro_saida.combo_caixa)
  #--preencher o campo-------
  @po.digitar(@cadastro_saida.input_caixa, "Buritis")
  #--seleciona a opcao-------
  @po.clicar_opcao(@cadastro_saida.opcao_caixa)
end


Quando('informar o valor #{string} saida') do |valor|
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  @po.digitar(@cadastro_saida.input_valor, valor)
end

Quando('informar a data do movimento #{string} saida') do |datamovimento|
  find("#id_data_movimento").set datamovimento
end

Quando('informar o Pagante\/Favorecido #{string} saida') do |pagante|
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  @po.digitar(@cadastro_saida.input_pagante_favorecido, pagante)
end

Quando('informar a descrição #{string} saida') do |descricao|
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  @po.digitar(@cadastro_saida.input_descricao, descricao)
end

Quando('selecionar o Plano conta saida #{string}') do |plano|
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  @po.clicar_combo(@cadastro_saida.combo_plano_conta)
  @po.digitar(@cadastro_saida.input_plano_conta, plano)
  @po.clicar_opcao(@cadastro_saida.opcao_plano_conta)
end

Quando("submeter o cadastro saida") do
  # botão submeter
  @po.clicar(@cadastro_saida.btn_salvar_cadastro)
end

Então("o sistema deve apresentar a mensagem de sucesso saida") do
  @po = PageObject.new
  @cadastro_saida = Cadastro_Saida.new
  @po.validar_mensagem(@cadastro_saida.msg_sucesso, "Lançamentos incluido com sucesso!")
end
