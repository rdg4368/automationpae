require "capybara"
require "capybara/cucumber"
require "capybara/rspec"
require "selenium-webdriver"
require "site_prism"
require "faker"
require "report_builder"

Capybara.register_driver :site_prism do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)  # Vamos utilizar o navegador chrome
end

# Configurando o driver
Capybara.configure do |config|
  config.run_server = false
  Capybara.default_driver = :site_prism
  Capybara.page.driver.browser.manage.window.maximize  # Maximizando a tela
  #Capybara.page.current_window.resize_to(1440, 900)
  config.app_host = "https://pae..com.br" # URL base que vamos utilizar
  config.default_max_wait_time = 10  # Tempo máximo que a automação vai esperar para a página carregar ou esperar um elemento
end

Before do
  @app = App.new
end

class App
  def login
    Login.new
  end

  def cadastro_entrada
    Cadastro_Entradas.new
  end

  def cadastro_saida
    Cadastro_Saida.new
  end

  def po
    PageObject.new
  end
end
